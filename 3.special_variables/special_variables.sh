#!/bin/bash
echo $0 # name of the program.
echo $1 # variables $1 through $9 show the nth argument provided.
echo $# # number of arguments
echo $@ # list with all the arguments
echo $? # exit status of the most recent process (here it will always be zero because the last echo will always work).
echo $$ # PID
echo $USER 
echo $HOSTNAME
echo $SECONDS # number of seconds since the script started
echo $RANDOM
echo $LINENO
