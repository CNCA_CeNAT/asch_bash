#!/bin/bash

# Assignment of a variable
VARIABLE="CNCA"


# Usage of a variable
echo $VARIABLE

# Also valid names
variable="PRIAS"
var_iable="Lanotec"
_variable="Cenibiot"

echo $variable
echo $var_iable
echo $_variable
